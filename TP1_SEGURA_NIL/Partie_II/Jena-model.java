/* Jena File from the semantic web course, #TP1
*
* 	By :
*  		-SEGURA_bastien (bastien.segura@etu.umontpellier.fr),
*  		-NIL_bahaa (bahaa-eddine.nil@etu.umontpellier.fr)
*
*  Work submitted to Moodle on 10 February 2020
*/

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;


public class jena_tp1 {

	public static void main(String[] args) {

		//Prefixes that will be used in this file :
		String dbpedia = "http://dbpedia.org/resource/";
		String dbpedia_owl = "http://dbpedia.org/ontology/";
		String music = "http://www.kanzaki.com/ns/music#";
		String open_vocab = "http://open.vocab.org/terms/";
		String music_ontology = "http://purl.org/ontology/mo/";
		String rdfs = "http://www.w3.org/2000/01/rdf-schema#";
		String rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		String whois = "http://www.kanzaki.com/ns/whois#";
		String event = "http://purl.org/NET/c4dm/event.owl#";
		String xsd = "http://www.w3.org/2001/XMLSchema#";

		//creating the model
		Model model = ModelFactory.createDefaultModel();

		//linking prefixes to the model
		model.setNsPrefix("dbpedia", dbpedia);
		model.setNsPrefix("dbpediaowl", dbpedia_owl);
		model.setNsPrefix("music", music);
		model.setNsPrefix("openvocab", open_vocab);
		model.setNsPrefix("musicontology", music_ontology);
		model.setNsPrefix("rdfs", rdfs);
		model.setNsPrefix("rdf", rdf);
		model.setNsPrefix("whois", whois);
		model.setNsPrefix("event", event);
		model.setNsPrefix("xsd", xsd);


		// declaring all the resources
		Resource dbpedia_Jupiter = model.createResource("dbpedia:Symphony_No._41_(Mozart)");
		Resource music_Symphony = model.createResource("music:Symphony");
		Resource music_Oeuvre = model.createResource("music:Oeuvre");	
		Resource dbpedia_Wolfgang_Amadeus_Mozart = model.createResource("dbpedia:Wolfgang_Amadeus_Mozart");
		Resource music_Composer = model.createResource("music:Composer");
		Resource dbpedia_Salzbourg = model.createResource("dbpedia:Salzburg");
		Resource dbpedia_Vienna = model.createResource("dbpedia:Vienna");
		Resource record = model.createResource();
		Resource music_ontology_Record = model.createResource("musicontology:Record");
		Resource dbpedia_London_Symphony_Orchestra = model.createResource("dbpedia:London_Symphony_Orchestra");
		Resource dbpedia_Claudio_Abbado = model.createResource("dbpedia:Claudio_Abbado");
		
		// declaring all the properties
		Property open_vocab_composedBy = model.createProperty("openvocab:composedBy");
		Property music_ontology_movement = model.createProperty("musicontology:movement");
		Property music_ontology_genre = model.createProperty("musicontology:genre");
		Property whois_born = model.createProperty("whois:born");
		Property dbpedia_owl_birthPlace = model.createProperty("dbpediaowl:birthPlace");
		Property dbpedia_owl_deathDate = model.createProperty("dbpediaowl:deathDate");
		Property dbpedia_owl_deathPlace = model.createProperty("dbpediaowl:deathPlace");
		Property dbpedia_owl_recordDate = model.createProperty("dbpediaowl:recordDate");
		Property dbpedia_owl_recordedIn = model.createProperty("dbpediaowl:recordedIn");
		Property music_conductor = model.createProperty("music:conductor");
		
		
		//linking the properties and the resources together ...
		dbpedia_Jupiter.addProperty(RDF.type, music_Oeuvre)
			.addProperty(music_ontology_genre, music_Symphony)
			.addProperty(open_vocab_composedBy,dbpedia_Wolfgang_Amadeus_Mozart)
			.addProperty(music_ontology_movement,"Allegro_Vivace")
			.addProperty(music_ontology_movement,"Andante_Cantabile")
			.addProperty(music_ontology_movement,"Menuetto")
			.addProperty(music_ontology_movement,"Molto_Allegro")
			.addProperty(RDFS.label,"Symphony_no_41")
			.addProperty(RDFS.label,"Jupiter");

		dbpedia_Wolfgang_Amadeus_Mozart.addProperty(RDF.type, music_Composer)
			.addProperty(whois_born,"27/01/1756")
			.addProperty(dbpedia_owl_birthPlace,dbpedia_Salzbourg)
			.addProperty(dbpedia_owl_deathDate, "05/12/1791")
			.addProperty(dbpedia_owl_deathPlace, dbpedia_Vienna);
		
		record.addProperty(RDF.type, music_ontology_Record)
			.addProperty(dbpedia_owl_recordDate,"1980")
			.addProperty(dbpedia_owl_recordedIn,dbpedia_London_Symphony_Orchestra)
			.addProperty(music_conductor,dbpedia_Claudio_Abbado);
		

		//... and displaying the model in a bunch of different formats !
		model.write(System.out,"JSON-LD");
		System.out.println("---------------------");
		model.write(System.out,"TTL");
		System.out.println("---------------------");
		model.write(System.out,"RDF/XML");

	}

}

